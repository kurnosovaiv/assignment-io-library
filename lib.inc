section .text

; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, 60     ; системный вызов 'exit'
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
.loop:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .loop
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length
	mov rsi, rdi
	mov rdi, 1
	mov rdx, rax
	mov rax, 1
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rsi, rsp
	mov rdi, 1
	mov rdx, 1
	mov rax, 1
	syscall
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 0xA
	call print_char
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	mov r8, rsp
	mov rdi, 10
	push 0

.loop:
	xor rdx, rdx
	div rdi
	add rdx, '0'
	dec rsp
	mov [rsp], dl
	test rax, rax
	jnz .loop

	mov rdi, rsp
	call print_string
	mov rsp, r8
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi
	jns .uns
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi

.uns:
	call print_uint    
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
.loop:
	mov al, byte [rsi]
	cmp al, byte [rdi]
	jne .fail
	cmp al, 0
	je .save
	inc rsi
	inc rdi
	jmp .loop
.fail:
	xor rax, rax
	ret
.save:
	xor rax, rax
	inc rax
	ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	mov rdi, 0
	mov rdx, 1
	push 0
	mov rsi, rsp
	syscall
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r12
	push r13
	push r14
	mov r12, rdi
	mov r13, rsi
	dec r13
	xor r14, r14

.loop:
	cmp r14, r13
	jg .fail
	call read_char
	cmp al, 0xA
	je .suc
	cmp al, 0x9
	je .suc
	cmp al, 0x20
	je .suc
	cmp al, byte 0
	je .end
	mov byte[r12+r14], al
	inc r14
	jmp .loop
    
.fail:
	xor rax, rax
	pop r14
	pop r13
	pop r12
	ret
    
.suc:
	cmp r14, 0
	je .loop
    
.end:
	mov [r12+r14], al
	mov rax, r12
	mov rdx, r14
	pop r14
	pop r13
	pop r12
	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:
	xor rax, rax
	xor rcx, rcx
	xor rsi, rsi
	mov r10, 10
.loop:
	mov sil, [rdi+rcx]
	sub rsi, '0'
	js .end
	cmp rsi, 9
	ja .end
	mul r10
	add rax, rsi
	inc rcx
	jmp .loop
.end:
	mov rdx, rcx
	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось

parse_int:
	xor rax, rax
	cmp byte [rdi], 0x2d
	je .parse
	call parse_uint
	ret
.parse:
	inc rdi
	call parse_uint
	cmp rdx, 0
	je .end
	neg rax
	inc rdx
.end:
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
	xor rax, rax
	xor rcx, rcx
	xor r9, r9
	push rdi
	call string_length
	pop rdi
	cmp rax, rdx
	jle .loop
	mov rax, 0
	ret
.loop:
	mov r9b, [rdi + rcx]
	mov [rsi + rcx], r9b
	inc rcx
	cmp byte[rsi + rcx], 0
	jne .loop
	mov rax, rdx
	ret
